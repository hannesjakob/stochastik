N <- 10000
X <- rpois(N, 1)
mu <- sum(X)/N
sigma <- sum((X-mu)^2)/(N-1)
hist(X)
message(mu)
message(sigma)